# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:F775C79E812553B6DCED5CC904908EBF49A20542]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/
